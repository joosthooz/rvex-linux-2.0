#
# LEON-2.x/Rules.make
#
# This file is included by the global makefile so that you can add your own
# platform-specific flags and dependencies.
#
# This file is subject to the terms and conditions of the GNU General Public
# License.  See the file "COPYING" in the main directory of this archive
# for more details.
#
# Copyright (c) 2001 by The LEOX team <team@leox.org>
#
# Based on arch/m68knommu/platform/5307/Makefile:
# Copyright (C) 1999 by Greg Ungerer (gerg@snapgear.com)
# Copyright (C) 1998,1999  D. Jeff Dionne <jeff@lineo.ca>
# Copyright (C) 1998       Kenneth Albanowski <kjahds@kjahds.com>
# Copyright (C) 1994 by Hamish Macdonald
# Copyright (C) 2000  Lineo Inc. (www.lineo.com) 
#

GCC_DIR = $(shell $(CC) -v 2>&1 | grep specs | sed -e 's/.* \(.*\)specs/\1\./')

INCGCC = $(GCC_DIR)/include
LIBGCC = #$(GCC_DIR)/libgcc.a

CFLAGS := $(CFLAGS) -fno-builtin -DNO_MM  -DMAGIC_ROM_PTR -DNO_FORGET $(VENDOR_CFLAGS) -DUTS_SYSNAME='"VEX-Linux"'
AFLAGS := $(CFLAGS) $(VENDOR_CFLAGS)

LINKFLAGS = -T arch/$(ARCH)/platform/$(PLATFORM)/$(BOARD)/elf32rvex.ld 


HEAD := arch/$(ARCH)/kernel/Head.o 

SUBDIRS := arch/$(ARCH)/kernel arch/$(ARCH)/mm arch/$(ARCH)/lib \
           arch/$(ARCH)/platform/$(PLATFORM) $(SUBDIRS)

ARCHIVES := arch/$(ARCH)/kernel/kernel.o arch/$(ARCH)/mm/mm.o \
            arch/$(ARCH)/platform/$(PLATFORM)/platform.o $(ARCHIVES)

LIBS += arch/$(ARCH)/lib/lib.a $(LIBGCC)

MAKEBOOT = $(MAKE) -C arch/$(ARCH)/boot


archclean:
	@$(MAKEBOOT) clean
	rm -f linux.s19
