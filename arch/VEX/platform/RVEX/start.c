
#include <asm/leon.h>
#include "hex.h"
#include "asm/system.h"
#include "asm/rvex.h"
#include "rvex_io.h"

#define UART_BASE 0xD1000000
#define UART_DATA (*((volatile unsigned char *)(UART_BASE)))
#define UART_STAT (*((volatile unsigned char *)(UART_BASE+4)))

/**
 * Prints a character to whatever platform the program is compiled for, if the
 * platform supports an output stream. Prototype conforms to the <stdio.h>
 * method.
 */
int lserial_putc(int character) {
  unsigned char c = character;

#ifndef SIM
  // Wait for the TX data FIFO ready flag.
  while (!(UART_STAT & (1 << 1)));
#endif

  // Write to the UART.
  UART_DATA = c;

  return 0;
}

/**
 * Same as putchar, but prints a null-terminated string. Prototype conforms to
 * the <stdio.h> method.
 */
int lserial_puts(const char *str) {
  while (*str) lserial_putc((int)(*str++));
  return 0;
}

/**
 * Reads a character from whatever input stream the platform has available,
 * waiting until one is available. Prototype conforms to the <stdio.h> method.
 */
int lserial_getc(void) {

  // Wait for the RX data ready flag.
  while (!(UART_STAT & (1 << 3)));

  // Read from the UART.
  return UART_DATA;

}

static unsigned long testvar = 0xdeadbeef;

extern void register_console(void (*proc)(const char *));

void _panic_handler(void);
void _trap_handler(void);

int main()
{
	extern int* __DATA_START, __DATA_END;
	int m;

#if 0     
        puts("Testing RAM\n");

        puts("Write...\n");
        for (dest = (unsigned long *)0x40000000; dest < (unsigned long *)0x40080000; dest++) {
                *dest = (unsigned long)0x5a5a5a5a ^ (unsigned long)dest;
        }
 
        puts("Read...\n");
        for (dest = (unsigned long *)0x40000000; dest < (unsigned long *)0x40080000; dest++) {
                tmp = (unsigned long)0x5a5a5a5a ^ (unsigned long)dest;
                if (*dest != tmp) {
                        puts("Failed.");
			outhex32((unsigned long)dest);
			puts("is");
			outhex32(*dest);
			puts("wrote");
			outhex32(tmp);
                        while(1);
                }
        }
#endif 


		while(UART_STAT & 8)
			lserial_getc();
    	lserial_putc('\n');

		CR_PH = (unsigned int)_panic_handler;
		CR_TH = (unsigned int)_trap_handler;

        if (testvar == 0xdeadbeef) lserial_puts("Found my key\n");
        else lserial_puts("My keys are missing!\n");

        start_kernel();

        return 0;
}

