/*
 * linux/arch/st200/kernel/sys_st200.c
 *
 * Copyright (C) 2003 STMicroelectronics Limited
 *	Author: Stuart Menefy <stuart.menefy@st.com>
 *
 * This file contains various system calls that have different calling
 * conventions on different platforms.
 */
#include <linux/config.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/sched.h>
#include <linux/file.h>		/* doh, must come after sched.h... */



/* Linux version of mmap */
asmlinkage unsigned long sys_mmap(unsigned long addr, unsigned long len,
	unsigned long prot, unsigned long flags, unsigned long fd,
	unsigned long off)
{
	struct file * file = NULL;
	long retval;

	if (!(flags & MAP_ANONYMOUS)) {
		if (fd >= NR_OPEN || !(file = current->files->fd[fd])){
			return -EBADF;
	    }
	}
	retval = do_mmap(file, addr, len, prot, flags, off);
	return retval;
}

/*
 * Perform the mmap() system calls.
 * Linux/m68k cloned Linux/i386, which didn't use to be able to
 * handle more than 4 system call parameters, so these system calls
 * used a memory block for parameter passing..
 */

asmlinkage int old_mmap(unsigned long *buffer)
{
	int error;
	unsigned long flags;
	struct file * file = NULL;

	error = verify_area(VERIFY_READ, buffer, 6*sizeof(long));
	if (error)
		return error;
	flags = get_user(buffer+3);
	if (!(flags & MAP_ANONYMOUS)) {
		unsigned long fd = get_user(buffer+4);
		if (fd >= NR_OPEN || !(file = current->files->fd[fd]))
			return -EBADF;
	}
	flags &= ~(MAP_EXECUTABLE | MAP_DENYWRITE);
	return do_mmap(file, get_user(buffer), get_user(buffer+1),
		       get_user(buffer+2), flags, get_user(buffer+5));
}

