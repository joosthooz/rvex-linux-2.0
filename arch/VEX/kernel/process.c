/*  $Id: process.c,v 1.51 1996/04/25 06:08:49 davem Exp $
 *  linux/arch/sparc/kernel/process.c
 *
 *  Copyright (C) 1995 David S. Miller (davem@caip.rutgers.edu)
 */

/*
 * This file handles the architecture-dependent parts of process handling..
 */

#define __KERNEL_SYSCALLS__
#include <stdarg.h>

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/stddef.h>
#include <linux/unistd.h>
#include <linux/ptrace.h>
#include <linux/malloc.h>
#include <linux/ldt.h>
/* #include <linux/user.h>	//H don't know if we need this, but it gives errors */
#include <linux/a.out.h>

#include <asm/segment.h>
#include <asm/system.h>
#include <asm/page.h>
#include <asm/pgtable.h>
#include <asm/delay.h>
#include <asm/processor.h>
#include <asm/system.h>
#include <asm/asm_offsets.h>

#include <asm/syscallparams.h>

int active_ds = KERNEL_DS; /* //H TODO: what does this do? (was USER_DS but we dont have usermode) */

int last_caller_of_sys_idle;

/*
 * the idle loop on a Sparc... ;)
 */
asmlinkage int sys_idle(void)
{
	volatile int i;
	//H our compiler doesn't support builtin return_address, so we need to use this asm stub
	__asm__ volatile (
	"\t c0 mov %0 = $l0.0\n"
	";;\n\t"
	:"=r" (last_caller_of_sys_idle)
//	:
//	:
	);

	if (current->pid != 0)
		return -EPERM;

	/* endless idle loop with no priority at all */
	current->counter = -100;
	for (;;) {
		//for (i = 2500; i > 0; i--) ; //H not too fast
		schedule();
	}
	return 0;
}

void hard_reset_now(void)
{
	volatile int i,j;
	for (j = 3; j > 0; j--)
	{
		printk("System will halt in %d seconds...\r", j);
		for (i = 3000000; i > 0; i--) ;
	}

	//while (1) ;

	__asm__ volatile (
		"stop"
/*			:
			:
			:*/
			);
}


/*
 * Free current thread data structures etc..
 */
void exit_thread(void)
{

}

/*
 * Free old dead task when we know it can never be on the cpu again.
 */
void release_thread(struct task_struct *dead_task)
{

}

void flush_thread(void)
{

}


/*
 * This is the mechanism for creating a new kernel thread.
 *
 * NOTE! Only a kernel-only process(ie the swapper or direct descendants
 * who haven't done an "execve()") should use this: it will work within
 * a system call from a "real" process, but the process memory space will
 * not be free'd until both the parent and the child have exited.
 */
pid_t kernel_thread(int (*fn)(void *), void * arg, unsigned long flags)
{
	/* This forms the data structure which copy_thread expects */
	struct {
		struct switch_stack ss;
		struct pt_regs regs;
	} regs_ss;
	struct pt_regs *regs = &regs_ss.regs;

	void kernel_thread_helper(void);

	memset(&regs_ss, 0, sizeof(regs_ss));
	regs_ss.ss.r57 = (unsigned long)arg;
	regs->lr = (unsigned long) fn;
	regs->pc = (unsigned long) kernel_thread_helper;
	regs->sccr = CR_CCR_KME | CR_CCR_IEN | CR_CCR_RFT;
	/* Ok, create the new process.. */
	return do_fork(flags | CLONE_VM, 0, regs);
}


#if 0
	__asm__ __volatile("mov %4, %%g2\n\t"    /* Set aside fn ptr... */
			   "mov %5, %%g3\n\t"    /* and arg. */
			   "mov %1, %%g1\n\t"
			   "mov %2, %%o0\n\t"    /* Clone flags. */
			   "mov 0, %%o1\n\t"     /* usp arg == 0 */
			   "t 0x10\n\t"          /* Linux/Sparc clone(). */
			   "cmp %%o1, 0\n\t"
			   "be 1f\n\t"           /* The parent, just return. */
			   " nop\n\t"            /* Delay slot. */
			   "jmpl %%g2, %%o7\n\t" /* Call the function. */
			   " mov %%g3, %%o0\n\t" /* Get back the arg in delay. */
			   "mov %3, %%g1\n\t"
			   "t 0x10\n\t"          /* Linux/Sparc exit(). */
			   /* Notreached by child. */
			   "1: mov %%o0, %0\n\t" :
			   "=r" (retval) :
			   "i" (__NR_clone), "r" (flags | CLONE_VM),
			   "i" (__NR_exit),  "r" (fn), "r" (arg) :
			   "g1", "g2", "g3", "o0", "o1", "memory");


/*
 * We assumne that this is always called as a result of a fork, vfork or
 * clone system call. In this case, immediatly below regs is a struct switch_stack,
 * which we copy onto the new thread's stack, ready for switch_to to load from.
 */
int copy_thread(int nr, unsigned long clone_flags, unsigned long sp,
	unsigned long unused,
	struct task_struct * p, struct pt_regs * regs)
{
	struct pt_regs * childregs;
	struct task_struct *tsk;
	struct switch_stack *childstack, *stack;
	asmlinkage void ret_from_fork(void);

	//printk("copy_thread: task_struct %08x\n", p);
	childregs = ((struct pt_regs *) (THREAD_SIZE + (unsigned long) p->thread_info)) - 1;
	*childregs = *regs;
	if (user_mode(regs)) {
		childregs->r12 = sp;
	} else {
		/* API gives called function a 16 byte scratch area */
		/* but it must also be 32 byte aligned */
		childregs->r12 = THREAD_SIZE + (unsigned long)p->thread_info - 32;
	}

	stack = ((struct switch_stack *) regs) - 1;
	childstack = ((struct switch_stack *) childregs) - 1;
	childstack = (struct switch_stack *)((unsigned int)childstack - 32); /* scratch area */
	*childstack = *stack;

	p->thread.sp = (unsigned long) childstack;
	p->thread.pc = (unsigned long) ret_from_fork;

	p->thread.flags = 0;

	tsk = current;

	/*
	 * Set a new TLS for the child thread?
	 */
	/*
	if (clone_flags & CLONE_SETTLS) {
		childstack->r13 = childregs->r20;
	}
	*/
	return 0;
}
#endif



extern void ret_from_fork(void);

void copy_thread(int nr, unsigned long clone_flags, unsigned long sp,
		 struct task_struct *p, struct pt_regs *regs)
{
//H todo: I don't know exactly what to do here
	struct pt_regs *childregs;
	unsigned long stack_offset;
	struct switch_stack *stack, *childstack;


	/* Calculate offset to stack_frame & pt_regs */
	stack_offset = ((PAGE_SIZE) - TRACEREG_SZ);

	childregs = ((struct pt_regs *) (p->kernel_stack_page + stack_offset-STACK_SCRATCH_AREA));
	*childregs = *regs;

	//switch_stack for __switch_to
	stack = ((struct switch_stack *) regs) - 1;
	childstack = ((struct switch_stack *) childregs) - 1;
	childstack = (struct switch_stack *)((unsigned int)childstack - STACK_SCRATCH_AREA); /* scratch area */
	*childstack = *stack;

	p->tss.sp = (unsigned long) childstack;
	p->tss.pc = (unsigned long) ret_from_fork;
	p->tss.flags = 0;


	/* Child returns 0 in ret_from_fork. */
//	childregs->r3 = 0;

	/* Set the return value for the parent. */
	regs->r3 = current->pid;

	if (sp) //clone
		childregs->r1 = sp;
	else
	{
		if (regs->r1) //vfork
			childregs->r1 = regs->r1;
		else //kernel_thread
			childregs->r1 = (p->kernel_stack_page + stack_offset-STACK_SCRATCH_AREA);
	}

}


/*
 * fill in the user structure for a core dump..
 */
void dump_thread(struct pt_regs * regs, struct user * dump)
{
#if 0
	//H this is for SPARC/SUNOS dumps, update to LX
	unsigned long first_stack_page;

	dump->magic = SUNOS_CORE_MAGIC;
	dump->len = sizeof(struct user);
	dump->regs.vcr = regs->vcr;
	dump->regs.pc = regs->pc;
//	dump->regs.sp = regs->sp;
	dump->regs.lr = regs->lr;
	/* fuck me plenty */
	memcpy(&dump->regs.r1, &regs->r1, (sizeof(unsigned long) * 63));
	memcpy(&dump->regs.b1, &regs->b1, (sizeof(unsigned char) * 8));
	dump->uexec = current->tss.core_exec;
	dump->u_tsize = (((unsigned long) current->mm->end_code) -
		((unsigned long) current->mm->start_code)) & ~(PAGE_SIZE - 1);
	dump->u_dsize = ((unsigned long) (current->mm->brk + (PAGE_SIZE-1)));
	dump->u_dsize -= dump->u_tsize;
	dump->u_dsize &= ~(PAGE_SIZE - 1);
	first_stack_page = (regs->u_regs[UREG_FP] & ~(PAGE_SIZE - 1));
	dump->u_ssize = (TASK_SIZE - first_stack_page) & ~(PAGE_SIZE - 1);

	dump->sigcode = current->tss.sig_desc;
#endif
}


void start_thread(struct pt_regs *regs, unsigned long pc, unsigned long sp)
{
	set_fs(USER_DS);
	regs->r1 = sp;
	regs->lr  = 0;
	regs->sccr = CR_CCR_IEN | CR_CCR_RFT | CR_CCR_KME_C;
	regs->pc  = pc;
}


asmlinkage int
st200_clone(unsigned long clone_flags, unsigned long newsp,
	    int *parent_tidptr, int *child_tidptr,
	    unsigned long tls_value,int arg8, int arg9, int arg10,
		struct syscallparams_ss params)
{
	struct pt_regs *regs = &params.regs;
#ifdef DEBUG
printk("sys_clone: pc %08x, sccr %08x, r1 %08x\n", regs->pc, regs->sccr, regs->r1);
printk("sys_clone: flags %08x newsp %08x\n", clone_flags, newsp);
#endif

	if (newsp == 0)
	{
//		while (1) ;
//		newsp = regs->r1; //H copy_thread will assign a new stack (right beneath our current stack)
	}

	return do_fork(clone_flags, newsp, regs);
}

int sys_execve(char *name, char **argv, char **envp,
	       int arg4,
	       int arg5, int arg6, int arg7, int arg8,
	       struct syscallparams syscallparams)
{
	char *filename;

	int error = getname (name, &filename);
	if(error)
		return error;
	struct pt_regs *regs = &syscallparams.regs;

	error = do_execve (filename, argv, envp, regs);
	putname (filename);

	return error;
}
