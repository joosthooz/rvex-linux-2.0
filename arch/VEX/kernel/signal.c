/*
 * linux/arch/st200/kernel/signal.c
 *
 * Copyright (C) 2003 STMicroelectronics Limited
 *	Author: Stuart Menefy <stuart.menefy@st.com>
 *
 */

#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/kernel.h>
#include <linux/signal.h>
#include <linux/errno.h>
#include <linux/wait.h>
#include <linux/unistd.h>

#include <linux/stddef.h>
//#include <linux/suspend.h>

#include <asm/syscallparams.h>

//#define DEBUG_SIG 1

#define _S(nr) (1<<((nr)-1))
#define _BLOCKABLE (~(sigmask(SIGKILL) | sigmask(SIGSTOP)))

static int do_signal(struct pt_regs *regs, struct switch_stack *ss,
		     int syscall, unsigned long syscall_r3, sigset_t * oldset);

/*
 * Atomically swap in the new signal mask, and wait for a signal.
 */

asmlinkage int
st200_rt_sigsuspend(sigset_t * unewset, size_t sigsetsize,
		    int arg3, int arg4,
		    int arg5, int arg6, int arg7, int arg8,
		    struct syscallparams_ss params)
{
	struct pt_regs *regs = &params.regs;
	struct switch_stack *ss = &params.ss;
	sigset_t saveset, newset;

#if DEBUG_SIG
	printk("SIG st200_rt_sigsuspend (current pid %d, size %d)\n",
	       current->pid, sigsetsize);
#endif

	/* XXX: Don't preclude handling different sized sigset_t's.  */
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	newset = *unewset;

	//sigdelsetmask(&newset, ~_BLOCKABLE);

	//spin_lock_irq(&current->sighand->siglock);
	saveset = current->blocked;
	current->blocked = newset & _BLOCKABLE;
	//recalc_sigpending();
	//spin_unlock_irq(&current->sighand->siglock);

	/*
	 * If we have got this far, then we will probably be returning
	 * via a signal handler, and so need to return via
	 * restore_all.  Indicate this by the "return 0"
	 * below. However this bypasses the normal error code
	 * handling, so set the error code here in regs directly.
	 */
	regs->r3 = EINTR;
	regs->br = 1;

	while (1) {
		current->state = TASK_INTERRUPTIBLE;
		schedule();
		if (do_signal(regs, ss, 0, 0, &saveset))
			return 0;
	}
}
/*
asmlinkage int
sys_sigaltstack(const stack_t * uss, stack_t * uoss,
		int arg3, int arg4,
		int arg5, int arg6, int arg7, int arg8,
		struct syscallparams params)
{
	struct pt_regs *regs = &params.regs;
	return do_sigaltstack(uss, uoss, regs->r1);
}
*/

struct sigframe {
	struct sigcontext sc;
	siginfo_t info;
	unsigned long retcode[4];
};

asmlinkage void st200_rt_sigreturn(struct sigcontext *sc, struct pt_regs *regs)
{
	sigset_t set;
	stack_t st;

#if DEBUG_SIG
	printk("SIG return (%s:%d): context=%p\n",
	       current->comm, current->pid, sc);
#endif

	if (verify_area(VERIFY_READ, sc, sizeof(*sc)))
		goto badframe;
//	if (__copy_from_user(&set, &sc->sc_mask, sizeof(set)))
//		goto badframe;

//	sigdelsetmask(&set, ~_BLOCKABLE);
//	spin_lock_irq(&current->sighand->siglock);
//	current->blocked = set;
	current->blocked = sc->sc_mask & _BLOCKABLE; //from sparc
//	recalc_sigpending();
//	spin_unlock_irq(&current->sighand->siglock);

//	if (__copy_from_user(regs, &sc->sc_regs, sizeof(*regs)))
//		goto badframe;

	memcpy(regs, &sc->sc_regs, sizeof(*regs));

//	if (__copy_from_user(&st, &sc->sc_stack, sizeof(st)))
//		goto badframe;
	/* It is more difficult to avoid calling this function than to
	   call it and ignore errors.  */
//	do_sigaltstack(&st, NULL, regs->r12);
	return;

badframe:
	force_sig(SIGSEGV, current);
}

static inline void *get_sigframe(struct sigaction *sa, struct pt_regs *regs,
				 size_t frame_size)
{
	unsigned long sp;

	/* Default to using normal stack */
	sp = regs->r1;

//H disabled this
//	if ((ka->sa.sa_flags & SA_ONSTACK) && (!sas_ss_flags(sp))) {
//		sp = current->sas_ss_sp + current->sas_ss_size;
//	}

	return (void *)((sp - frame_size) & (~31ul));
}

static int
setup_sigcontext(struct sigcontext *sc,
		 struct pt_regs *regs, struct switch_stack *ss,
		 stack_t * stack, sigset_t * set)
{
//H disabling the checks (we don't have memory protection and separate address spaces anyway)
//	if (
			__put_user(0, &sc->sc_flags, sizeof(long));
//			)
//		return 1;
//	if (
			//__put_user(&sc->sc_regs, regs, sizeof(*regs));
			memcpy(&sc->sc_regs, regs, sizeof(*regs));
//			memcpy(&sc->sc_regs.r57, ss, sizeof(*ss)); //H trying to copy the callee-saved so they will be restored by restore_state_and_return
//			)
//		return 1;
//	if (
//	__copy_to_user(&sc->sc_ss, ss, sizeof(*ss))
			memcpy(&sc->sc_ss, ss, sizeof(*ss));
//	)
//		return 1;
//	if (
			//__put_user(&sc->sc_stack, stack, sizeof(*stack));
			memcpy(&sc->sc_stack, stack, sizeof(*stack));
//			)
//		return 1;
//	if (
			//__put_user(&sc->sc_mask, set, sizeof(*set));
			memcpy(&sc->sc_mask, set, sizeof(*set));
//			)
//		return 1;

	return 0;
}

static void
setup_frame(struct sigaction *sa, //siginfo_t * info,
	    struct pt_regs *regs, struct switch_stack *ss, int sig, sigset_t * set)
{
	struct sigframe *frame;
	stack_t stack;
	int err = 0;
	unsigned long sp;

	frame = get_sigframe(sa, regs, sizeof(*frame));

	if (verify_area(VERIFY_WRITE, frame, sizeof(*frame)))
		goto give_sigsegv;

//H don't know what these do and whether we need them
//	stack.ss_sp = (void *)current->sas_ss_sp;
//	stack.ss_size = current->sas_ss_size;
//	stack.ss_flags = sas_ss_flags(regs->r1);
	err |= setup_sigcontext(&frame->sc, regs, ss, &stack, set);
	if (err)
		goto give_sigsegv;

//H copy_siginfo_to_user seems to be newer than 2.0, and for rt signals. If we don't use rt, there's no info.
//	if (info) {
//		err |= copy_siginfo_to_user(&frame->info, info);
//		if (err)
//			goto give_sigsegv;
//	}

//	if (!vdso_sigrestorer && current->mm->context.vdso)
//		goto give_sigsegv;

	ss->r57 = (unsigned long)&frame->sc;

	register long _sc_addr asm("r0.15");
	_sc_addr = (unsigned long)&frame->sc;
//	frame->retcode[0] = 0x620601e0; //add	r0.3 = r0.0, r0.15 //not needed anymore because we're saving a pointer to the sigcontext in r57 in setup_frame
	frame->retcode[0] = 0x60000000; //nop
	frame->retcode[1] = 0x628401de; //add	r0.2 = r0.0, 0x77;;
	frame->retcode[2] = 0x60000000; //nop
	frame->retcode[3] = 0x90800242; //trap	Cause =  0x90, Argument =  r0.0;;

	regs->lr = frame->retcode;


	/* Work out the new sp */
	sp = (unsigned long)frame;
	sp -= 16;		/* Allow for scratch area */
	sp &= ~31ul;		/* and align down */

	/* Set up registers for signal handler */
	regs->r1 = sp;
//	regs->pc = (unsigned long)sa->sa_handler;
//	regs->pc = current->sig->action; //H from SPARC 2.0 nommu, not needed because do_signal takes care of this
	regs->r3 = sig;	/* arg0: signal number */
//	regs->r4 = (unsigned long)&frame->info;	/* arg1: siginfo */
//	regs->r5 = (unsigned long)&frame->sc;	/* arg2: sigcontext pointer */
	regs->r4 = (unsigned long)&frame->sc;	/* arg1: sigcontext pointer */
//	regs->r57 = (unsigned long)&frame->sc;	/* arg1: sigcontext pointer */

#if DEBUG_SIG
	printk("SIG deliver (%s:%d): frame=%p, sp=%p pc=%p ha=%p ra=%p\n",
	       current->comm, current->pid, frame, sp, regs->pc, current->sig->action, regs->lr);
#endif

	return;

give_sigsegv:
	do_exit(SIGSEGV);

}

/*
 * OK, we're invoking a handler
 */
#if 0 //st200
static void
handle_signal(unsigned long sig, struct sigaction *sa,
	      siginfo_t * info, sigset_t * oldset,
	      struct pt_regs *regs)
{

#if DEBUG_SIG
	printk("SIG handle_signal (pid %d)\n",
	       current->pid);
#endif

	/* Set up the stack frame */
	setup_frame(sig, sa,
		    (sa->sa_flags & SA_SIGINFO) ? info : NULL,
		    oldset, regs);

	if (sa->sa_flags & SA_ONESHOT)
		sa->sa_handler = SIG_DFL;

	if (!(sa->sa_flags & SA_NODEFER)) {
//		spin_lock_irq(&current->sighand->siglock);
//		sigorsets(&current->blocked, &current->blocked,
//			  &sa->sa.sa_mask);
//		sigaddset(&current->blocked, sig);
//		recalc_sigpending();
//		spin_unlock_irq(&current->sighand->siglock);
		current->blocked |= sa->sa_mask|sig;
	}
}

//static inline void
//syscall_restart(struct k_sigaction *sa,
//		struct pt_regs *regs,
//		unsigned long syscall_r3)
//{
//	/* Check system call restarting.. */
//	switch (regs->r3) {
//	case ERESTART_RESTARTBLOCK:
////		current_thread_info()->restart_block.fn = do_no_restart_syscall; //H No idea what this does
//		/* fall through */
//
//	case ERESTARTNOHAND:
//		regs->r3 = EINTR;
//		break;
//
//	case ERESTARTSYS:
//		if (!(sa->sa_flags & SA_RESTART)) {
//			regs->r3 = EINTR;
//			break;
//		}
//		/* fall through */
//
//	case ERESTARTNOINTR:
//		/* repeat the syscall */
//		regs->r3 = syscall_r3;
//		//regs->pc -= 4; //H TODO: we should use the original trap point. normally, in syscall exit, we find the next bundle and return to that. Now, we should return through restore_state_and_return.
//		break;
//	}
//}
#endif

/*
 * Note that 'init' is a special process: it doesn't get signals it doesn't
 * want to handle. Thus you cannot kill init even with a SIGKILL even by
 * mistake.
 *
 * Remember this is called from st200_rt_sigsuspend, as well as
 * do_notify_resume.
 *
 * syscall is 1 if we came from a system call which errored, 0 otherwise.
 * If syscall is 1, then syscall_r16 will contain the r16 on entry to
 * the system call, in case we need to restart.
 */
static int do_signal(struct pt_regs *regs, struct switch_stack *ss, int syscall, unsigned long syscall_r3, sigset_t * oldmask ) //oldmask was oldset in st200
{
	unsigned long mask = ~current->blocked;
	unsigned long handler_signal = 0;
	struct sigcontext_struct *frame = NULL;
	unsigned long pc = 0;
	unsigned long npc = 0;
	unsigned long signr;
	struct sigaction *sa;

	while ((signr = current->signal & mask) != 0) {
		signr = ffz(~signr);
		clear_bit(signr, &current->signal);
		sa = current->sig->action + signr;
		signr++;
		if ((current->flags & PF_PTRACED) && signr != SIGKILL) {
			current->exit_code = signr;
			current->state = TASK_STOPPED;
			notify_parent(current, SIGCHLD);
			schedule();
			if (!(signr = current->exit_code))
				continue;
			current->exit_code = 0;
			if (signr == SIGSTOP)
				continue;
			if (_S(signr) & current->blocked) {
				current->signal |= _S(signr);
				continue;
			}
			sa = current->sig->action + signr - 1;
		}
		if(sa->sa_handler == SIG_IGN) {
			if(signr != SIGCHLD)
				continue;
			while(sys_waitpid(-1,NULL,WNOHANG) > 0);
			continue;
		}
		if(sa->sa_handler == SIG_DFL) {
			if(current->pid == 1)
				continue;
			switch(signr) {
			case SIGCONT: case SIGCHLD: case SIGWINCH:
				continue;

			case SIGSTOP: case SIGTSTP: case SIGTTIN: case SIGTTOU:
				current->state = TASK_STOPPED;
				current->exit_code = signr;
				if(!(current->p_pptr->sig->action[SIGCHLD-1].sa_flags &
				     SA_NOCLDSTOP))
					notify_parent(current,  SIGCHLD);
				schedule();
				continue;

			case SIGQUIT: case SIGILL: case SIGTRAP:
			case SIGABRT: case SIGFPE: case SIGSEGV:
				if(current->binfmt && current->binfmt->core_dump) {
					if(current->binfmt->core_dump(signr, regs))
						signr |= 0x80;
				}
				/* fall through */
			default:
				current->signal |= _S(signr & 0x7f);
				current->flags |= PF_SIGNALED;
				do_exit(signr);
			}
		}
		/* OK, we're invoking a handler. */
		//H disable this for now, has something to do with interrupting a syscall but it uses the carry bit (I'd think it would use the supervisor bit)
//		if(regs->psr & PSR_C) {
//			if(regs->u_regs[UREG_I0] == ERESTARTNOHAND ||
//			   (regs->u_regs[UREG_I0] == ERESTARTSYS && !(sa->sa_flags & SA_RESTART)))
//				regs->u_regs[UREG_I0] = EINTR;
//		}
		handler_signal |= 1 << (signr - 1);
		mask &= ~sa->sa_mask;
	}
	//same as above
//	if((regs->psr & PSR_C) &&
//	   (regs->u_regs[UREG_I0] == ERESTARTNOHAND ||
//	    regs->u_regs[UREG_I0] == ERESTARTSYS ||
//	    regs->u_regs[UREG_I0] == ERESTARTNOINTR)) {
//		/* replay the system call when we are done */
//		regs->u_regs[UREG_I0] = regs->u_regs[UREG_G0];
//		regs->pc -= 4;
//		regs->npc -= 4;
//	}
	if(!handler_signal)
		return 0;
	pc = regs->pc;
//	npc = regs->npc;
//	frame = (struct sigcontext_struct *) regs->sp;
	signr = 1;
	sa = current->sig->action;
	for(mask = 1; mask; sa++, signr++, mask += mask) {
		if(mask > handler_signal)
			break;
		if(!(mask & handler_signal))
			continue;
		setup_frame(sa, regs, ss, signr, oldmask);
		pc = (unsigned long) sa->sa_handler;
//		npc = pc + 4;
		if(sa->sa_flags & SA_ONESHOT)
			sa->sa_handler = NULL;
		current->blocked |= sa->sa_mask;
		*oldmask |= sa->sa_mask;
	}
	regs->pc = pc;
//	regs->npc = npc;
	return 1;

#if 0 //st200

	siginfo_t info;
	int signr;
	struct sigaction sa;

#if DEBUG_SIG
	printk("SIG do_signal (pid %d)\n",
	       current->pid);
#endif

	if (!oldset)
		oldset = &current->blocked;

//	signr = get_signal_to_deliver(&info, &sa, regs, NULL);
	signr = current->signal & ~current->blocked;

	if (signr > 0) {
		/* Whee!  Actually deliver the signal.  */
//		if (syscall)
//			syscall_restart(&sa, regs, syscall_r3);
		handle_signal(signr, &sa, &info, oldset, regs);
		return 1;
	}

	//H We're not supporting restarting syscalls at this time

	/* Did we come from a system call which errored? */
//	if (syscall) {
//		/* Restart the system call - no handlers present */
//		switch (regs->r3) {
//		case ERESTARTNOHAND:
//		case ERESTARTSYS:
//		case ERESTARTNOINTR:
//			regs->r3 = syscall_r3;
////			regs->pc -= 4; //H TODO same as above
//			break;
//
//		case ERESTART_RESTARTBLOCK:
//			regs->r2 = __NR_restart_syscall;
////			regs->pc -= 4; //H TODO same as above
//			break;
//		}
//	}
	return 0;

#endif
}

/*
 * notification of userspace execution resumption
 * - triggered by current->work.notify_resume
 * interrupts are still disabled
 */
void do_notify_resume(struct pt_regs *regs, struct switch_stack *ss, int syscall, unsigned long syscall_r3,
		      sigset_t * oldset)
{
#if DEBUG_SIG
	printk
	    ("pid %d - do_notify_resume(regs 0x%x, syscall %d, r3 0x%x, current.signal %x, current.blocked %x\n",
	     current->pid, regs, syscall, syscall_r3, current->signal, current->blocked);
#endif
//	if (thread_info_flags & _TIF_SIGPENDING) { //H I don't think we have the same thread_info_flags
	if (current->signal & ~current->blocked) {
//		printk("I should be handling a signal now, but I'm not\n");
		do_signal(regs, ss, syscall, syscall_r3, oldset);
	}
}


//H sparcnommu version of signal.c:
#if 0

#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/signal.h>
#include <linux/errno.h>
#include <linux/wait.h>
#include <linux/ptrace.h>
#include <linux/unistd.h>
#include <linux/mm.h>

#include <asm/segment.h>
#include <asm/bitops.h>
#include <asm/ptrace.h>

#define _S(nr) (1<<((nr)-1))

#define _BLOCKABLE (~(_S(SIGKILL) | _S(SIGSTOP)))

asmlinkage int sys_waitpid(pid_t pid, unsigned long *stat_addr, int options);
asmlinkage int do_signal(unsigned long oldmask, struct pt_regs * regs);

/*
 * atomically swap in the new signal mask, and wait for a signal.
 * This is really tricky on the Sparc, watch out...
 */
asmlinkage inline void _sigpause_common(unsigned int set, struct pt_regs *regs)
{
	unsigned long mask;

	mask = current->blocked;
	current->blocked = set & _BLOCKABLE;

	/* Advance over the syscall instruction for when
	 * we return.  We want setup_frame to save the proper
	 * state, including the error return number & condition
	 * codes.
	 */
	regs->pc = regs->npc;
	regs->npc += 4;
	regs->psr |= PSR_C;
	regs->u_regs[UREG_I0] = EINTR;

	while (1) {
		current->state = TASK_INTERRUPTIBLE;
		schedule();
		if (do_signal(mask, regs))
			return;
	}
}

asmlinkage void do_sigpause(unsigned int set, struct pt_regs *regs)
{
	_sigpause_common(set, regs);
}

asmlinkage void do_sigsuspend (struct pt_regs *regs)
{
	unsigned long mask;
	unsigned long set;

	set = regs->u_regs [UREG_I0];
	mask = current->blocked;
	current->blocked = set & _BLOCKABLE;
	regs->pc = regs->npc;
	regs->npc += 4;
	while (1) {
		current->state = TASK_INTERRUPTIBLE;
		schedule();
		if (do_signal(mask,regs)){
			regs->psr |= PSR_C;
			regs->u_regs [UREG_I0] = EINTR;
			return;
		}
	}
}

asmlinkage void do_sigreturn(struct pt_regs *regs)
{
	struct sigcontext_struct *scptr =
		(struct sigcontext_struct *) regs->u_regs[UREG_I0];

	synchronize_user_stack();

	/* Check sanity of the user arg. */
	if(verify_area(VERIFY_READ, scptr, sizeof(struct sigcontext_struct)) ||
	   ((((unsigned long) scptr)) & 0x3)) {
		printk("%s [%d]: do_sigreturn, scptr is invalid at pc<%08lx> scptr<%p>\n",
		       current->comm, current->pid, regs->pc, scptr);
		do_exit(SIGSEGV);
	}

	if((scptr->sigc_pc | scptr->sigc_npc) & 3)
		return; /* Nice try. */

	current->blocked = scptr->sigc_mask & _BLOCKABLE;
	current->tss.sstk_info.cur_status = (scptr->sigc_onstack & 1);
	regs->pc = scptr->sigc_pc;
	regs->npc = scptr->sigc_npc;
	regs->u_regs[UREG_FP] = scptr->sigc_sp;
	regs->u_regs[UREG_I0] = scptr->sigc_o0;
	regs->u_regs[UREG_G1] = scptr->sigc_g1;

	/* User can only change condition codes in %psr. */
	regs->psr &= (~PSR_ICC);
	regs->psr |= (scptr->sigc_psr & PSR_ICC);
}

/*
 * Set up a signal frame... Make the stack look the way SunOS
 * expects it to look which is basically:
 *
 * ---------------------------------- <-- %sp at signal time
 * Struct sigcontext
 * Signal address
 * Ptr to sigcontext area above
 * Signal code
 * The signal number itself
 * One register window
 * ---------------------------------- <-- New %sp
 */
struct signal_sframe {
	struct reg_window sig_window;
	int sig_num;
	int sig_code;
	struct sigcontext_struct *sig_scptr;
	int sig_address;
	struct sigcontext_struct sig_context;
};
/* To align the structure properly. */
#define SF_ALIGNEDSZ  (((sizeof(struct signal_sframe) + 7) & (~7)))

static inline void
setup_frame(struct sigaction *sa, struct sigcontext_struct **fp,
	    unsigned long pc, unsigned long npc, struct pt_regs *regs,
	    int signr, unsigned long oldmask)
{
	struct signal_sframe *sframep;
	struct sigcontext_struct *sc;
	int window = 0;
	int old_status = current->tss.sstk_info.cur_status;

	synchronize_user_stack();
	sframep = (struct signal_sframe *) *fp;
	sframep = (struct signal_sframe *) (((unsigned long) sframep)-SF_ALIGNEDSZ);
	sc = &sframep->sig_context;
	if(verify_area(VERIFY_WRITE, sframep, sizeof(*sframep)) ||
	   (((unsigned long) sframep) & 7) ||
	   (((unsigned long) sframep) >= KERNBASE) ||
	   ((sparc_cpu_model == sun4 || sparc_cpu_model == sun4c) &&
	    ((unsigned long) sframep < 0xe0000000 && (unsigned long) sframep >= 0x20000000))) {
#if 0 /* fills up the console logs... */
		printk("%s [%d]: User has trashed signal stack\n",
		       current->comm, current->pid);
		printk("Sigstack ptr %p handler at pc<%08lx> for sig<%d>\n",
		       sframep, pc, signr);
#endif
		/* Don't change signal code and address, so that
		 * post mortem debuggers can have a look.
		 */
		current->sig->action[SIGILL-1].sa_handler = SIG_DFL;
		current->blocked &= ~(1<<(SIGILL-1));
		send_sig(SIGILL,current,1);
		return;
	}
	*fp = (struct sigcontext_struct *) sframep;

	sc->sigc_onstack = old_status;
	sc->sigc_mask = oldmask;
	sc->sigc_sp = regs->u_regs[UREG_FP];
	sc->sigc_pc = pc;
	sc->sigc_npc = npc;
	sc->sigc_psr = regs->psr;
	sc->sigc_g1 = regs->u_regs[UREG_G1];
	sc->sigc_o0 = regs->u_regs[UREG_I0];
	sc->sigc_oswins = current->tss.w_saved;
	if(current->tss.w_saved)
		for(window = 0; window < current->tss.w_saved; window++) {
			sc->sigc_spbuf[window] =
				(char *)current->tss.rwbuf_stkptrs[window];
			memcpy(&sc->sigc_wbuf[window],
			       &current->tss.reg_window[window],
			       sizeof(struct reg_window));
		}
	else
		memcpy(sframep, (char *)regs->u_regs[UREG_FP],
		       sizeof(struct reg_window));

	current->tss.w_saved = 0; /* So process is allowed to execute. */
	sframep->sig_num = signr;
	if(signr == SIGSEGV ||
	   signr == SIGILL ||
	   signr == SIGFPE ||
	   signr == SIGBUS ||
	   signr == SIGEMT) {
		sframep->sig_code = current->tss.sig_desc;
		sframep->sig_address = current->tss.sig_address;
	} else {
		sframep->sig_code = 0;
		sframep->sig_address = 0;
	}
	sframep->sig_scptr = sc;
	regs->u_regs[UREG_FP] = (unsigned long) *fp;
}

/*
 * Note that 'init' is a special process: it doesn't get signals it doesn't
 * want to handle. Thus you cannot kill init even with a SIGKILL even by
 * mistake.
 *
 * Note that we go through the signals twice: once to check the signals that
 * the kernel can handle, and then we build all the user-level signal handling
 * stack-frames in one go after that.
 */

asmlinkage int do_signal(unsigned long oldmask, struct pt_regs * regs)
{
	unsigned long mask = ~current->blocked;
	unsigned long handler_signal = 0;
	struct sigcontext_struct *frame = NULL;
	unsigned long pc = 0;
	unsigned long npc = 0;
	unsigned long signr;
	struct sigaction *sa;

	while ((signr = current->signal & mask) != 0) {
		signr = ffz(~signr);
		clear_bit(signr, &current->signal);
		sa = current->sig->action + signr;
		signr++;
		if ((current->flags & PF_PTRACED) && signr != SIGKILL) {
			current->exit_code = signr;
			current->state = TASK_STOPPED;
			notify_parent(current, SIGCHLD);
			schedule();
			if (!(signr = current->exit_code))
				continue;
			current->exit_code = 0;
			if (signr == SIGSTOP)
				continue;
			if (_S(signr) & current->blocked) {
				current->signal |= _S(signr);
				continue;
			}
			sa = current->sig->action + signr - 1;
		}
		if(sa->sa_handler == SIG_IGN) {
			if(signr != SIGCHLD)
				continue;
			while(sys_waitpid(-1,NULL,WNOHANG) > 0);
			continue;
		}
		if(sa->sa_handler == SIG_DFL) {
			if(current->pid == 1)
				continue;
			switch(signr) {
			case SIGCONT: case SIGCHLD: case SIGWINCH:
				continue;

			case SIGSTOP: case SIGTSTP: case SIGTTIN: case SIGTTOU:
				current->state = TASK_STOPPED;
				current->exit_code = signr;
				if(!(current->p_pptr->sig->action[SIGCHLD-1].sa_flags &
				     SA_NOCLDSTOP))
					notify_parent(current,  SIGCHLD);
				schedule();
				continue;

			case SIGQUIT: case SIGILL: case SIGTRAP:
			case SIGABRT: case SIGFPE: case SIGSEGV:
				if(current->binfmt && current->binfmt->core_dump) {
					if(current->binfmt->core_dump(signr, regs))
						signr |= 0x80;
				}
				/* fall through */
			default:
				current->signal |= _S(signr & 0x7f);
				current->flags |= PF_SIGNALED;
				do_exit(signr);
			}
		}
		/* OK, we're invoking a handler. */
		if(regs->psr & PSR_C) {
			if(regs->u_regs[UREG_I0] == ERESTARTNOHAND ||
			   (regs->u_regs[UREG_I0] == ERESTARTSYS && !(sa->sa_flags & SA_RESTART)))
				regs->u_regs[UREG_I0] = EINTR;
		}
		handler_signal |= 1 << (signr - 1);
		mask &= ~sa->sa_mask;
	}
	if((regs->psr & PSR_C) &&
	   (regs->u_regs[UREG_I0] == ERESTARTNOHAND ||
	    regs->u_regs[UREG_I0] == ERESTARTSYS ||
	    regs->u_regs[UREG_I0] == ERESTARTNOINTR)) {
		/* replay the system call when we are done */
		regs->u_regs[UREG_I0] = regs->u_regs[UREG_G0];
		regs->pc -= 4;
		regs->npc -= 4;
	}
	if(!handler_signal)
		return 0;
	pc = regs->pc;
	npc = regs->npc;
	frame = (struct sigcontext_struct *) regs->u_regs[UREG_FP];
	signr = 1;
	sa = current->sig->action;
	for(mask = 1; mask; sa++, signr++, mask += mask) {
		if(mask > handler_signal)
			break;
		if(!(mask & handler_signal))
			continue;
		setup_frame(sa, &frame, pc, npc, regs, signr, oldmask);
		pc = (unsigned long) sa->sa_handler;
		npc = pc + 4;
		if(sa->sa_flags & SA_ONESHOT)
			sa->sa_handler = NULL;
		current->blocked |= sa->sa_mask;
		oldmask |= sa->sa_mask;
	}
	regs->pc = pc;
	regs->npc = npc;
	return 1;
}

asmlinkage int
sys_sigstack(struct sigstack *ssptr, struct sigstack *ossptr)
{
	/* First see if old state is wanted. */
	if(ossptr) {
		if(verify_area(VERIFY_WRITE, ossptr, sizeof(struct sigstack)))
			return -EFAULT;
		memcpy(ossptr, &current->tss.sstk_info, sizeof(struct sigstack));
	}

	/* Now see if we want to update the new state. */
	if(ssptr) {
		if(verify_area(VERIFY_READ, ssptr, sizeof(struct sigstack)))
			return -EFAULT;
		memcpy(&current->tss.sstk_info, ssptr, sizeof(struct sigstack));
	}
	return 0;
}

#endif
