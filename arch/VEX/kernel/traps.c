
/*
 * Traps are not enabled in the Head assembly code, because its
 * not necessary for running C code properly (as with SPARC because of
 * its registry windows that use traps).
 * When we implement traps in hardware that will change
 */

#include <asm/system.h>

void trap_init(void)
{
	int i;
	cli();
	udelay(10);
	traps_enable();
}



/*
 * when implementing traps in hardware, their handlers will be implemented here
 * (like privileged/illegal instruction, functional unit unavailable)
 */








/* ST200 function to print registers */
void st200_show_regs(struct pt_regs* regs)
{
        int r;
        unsigned long* rp;
        int i;

        printk(" pc=%08lx  vcr=%08lx\n",
               regs->pc, regs->sccr);
        printk("br=%ld\n", ((unsigned char)regs->br));


        for (r=1, rp=&regs->r1; r<56; r+=4, rp+=4) {
                printk(" r%d=%08lx  r%d=%08lx  r%d=%08lx  r%d=%08lx\n",
                       r, rp[0], r+1, rp[1], r+2, rp[2], r+3, rp[3]);
        }
        printk(" lr=%08lx\n",
               regs->lr);
}
/* Used by sysrq-p, among others. struct switch_stack is not available */
void show_regs(struct pt_regs *regs)
{
	st200_show_regs(regs);
}
