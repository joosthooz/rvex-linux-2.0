/*
 * weird stuff
 */

#include <asm/unistd.h>
#include <asm/syscallparams.h>
#include <asm/rvex.h>


__asm__ ("_panic_handler::\n\
mov $r0.1 = 0x1000\n\
;;\n\
call $l0.0=real_panic_handler\n\
;;\n\
stop\n\
;;\n");

void panic_handler(void);

void real_panic_handler()
{
	char cause = CR_TC;
	void *point = CR_TP;
	unsigned int arg = CR_TA;
	printk("unhandled exception:\n");
	switch (cause){
		case 1:
			printk("Invalid opcode at %p\n", point);
			break;
		case 2:
			printk("Misaligned branch at %p to %p\n", point, (void *)arg);
			break;
		case 3:
			printk("Fetch fault at %p\n", point);
			break;
		case 4:
			printk("Misaligned data access at %p to %p\n", point, (void *)arg);
			break;
		case 5:
			printk("Data fault at %p to %p\n", point, (void *)arg);
			break;
		case 6:
			printk("Invalid long immediate encoding at %p", point);
			break;
		case 7:
			printk("Unhandled interrupt (%x) at %p\n", arg, point);
			break;
		default:
			printk("Unknown exception: %x at %p with argument %x\n", cause, point, arg);
			break;
	}
	__asm__ volatile("stop");
}

/*H from include/asm/unistd.h*/
unsigned long __syscall0(unsigned long nr)
{
	if (nr == __NR_idle)
		return sys_idle();
	if (nr == __NR_fork)
		return sys_vfork();
	if (nr == __NR_pause)
		return sys_pause();
	if (nr == __NR_setup)
		return sys_setup();
	if (nr == __NR_sync)
		return sys_sync();
	if (nr == __NR_setsid)
		return sys_setsid();
	printk("no such syscall\n");
	hard_reset_now();
	return 0;
}

unsigned long __syscall1(unsigned long p1, unsigned long nr)
{
	if (nr == __NR_dup)
		return sys_dup(p1);
	if (nr == __NR_close)
		return sys_close(p1);
	if (nr == __NR_exit)
		return sys_exit(p1);
	printk("no such syscall\n");
	hard_reset_now();
	return 0;
}

unsigned long __syscall2(unsigned long p1, unsigned long p2,
				unsigned long nr)
{
	printk("no such syscall\n");
	hard_reset_now();
	return 0;
}

unsigned long __syscall3(unsigned long p1, unsigned long p2,
				unsigned long p3, unsigned long nr)
{
	if (nr == __NR_open)
		return sys_open(p1, p2, p3);
	if (nr == __NR_write)
		return sys_write(p1, p2, p3);
	if (nr == __NR_execve)
	{
		return kernel_execve(p1, p2, p3);
	}
	if (nr == __NR_waitpid)
		return sys_waitpid(p1, p2, p3);
	printk("no such syscall\n");
	hard_reset_now();
	return 0;
}

unsigned long __syscall4(unsigned long p1, unsigned long p2,
				unsigned long p3, unsigned long p4,
				unsigned long nr)
{
	printk("syscall4\n");
	hard_reset_now();
	return 0;
}






void setpointer(void* pointer, void* value)
{
	pointer = value;
}


void sys_ptrace()
{
	panic("sys_ptrace not implemented\n");
}

/*sys_execve*/

void sys_execv()
{
	panic("sys_execv not implemented\n");
}
/*
int sys_pipe(int *fd)
{
	return do_pipe(fd);
}
*/

/*sys_sigpause*/
/*
void sys_sigsuspend()
{
	panic("sys_sigsuspend not implemented (use rt_sigsuspend)\n");
}

void sys_sigreturn()
{
	panic("sys_sigreturn not implemented (use rt_sigreturn)\n");
}
*/

/*sys_vfork*/

/*
void sys_clone()
{
	panic("sys_clone not implemented\n");
}
*/

