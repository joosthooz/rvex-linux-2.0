This linux kernel contains the VEX architecture. It is intended to be used with uCLinux. Download the distribution and copy this version of linux into the extracted uCLinux directory. The builtin Make rules will not work with our toolchain (we need separate steps for compilation and assembly), so it needs to be built with 'make -r'. You will probably also need the Vendor configuration files and the toolchain. contact us to get them at j.j.hoozemans@tudelft.nl.

User manual:
To run programs on rvex-linux, you need the following:
-rvex-rewrite with a bitstream on a FPGA (ml605) and the tools rvd and rvsrv running.
-A floating point library (we use FLIP for open64) in rvex-rewrite.
-The toolchain; open64, binutils-gdb and elf2bflt, grmon2.
-The full uCLinux distribution where you must replace the uClibc and linux-2.0.x with the rVEX versions
-The TUDelft Vendor files to add to uCLinux

You can choose to include your program in the build environment of uCLinux, or to use a rvex_root script (that sets all the correct compiler flags) or newlib (which is also included in rvex-rewrite but this doesn't use Linux).

If you have included the app in uCLinux, it will generate a binary and include it in the filesystem automatically. In this case, go into the libc directory and do:
make menuconfig
To start the menu to configure uCLibc. You will need to define the location of the kernel headers under the architecture settings. This needs to be a full path (not relative). As for the other settings here; there is no MMU, you can choose to enable floating point but it is a software emulation library anyway and I think it might break printf (maybe only for floats. Actually, maybe floats don't work at all in our uClibc. Leaving them disabled is probably the safest choice but you may not be able to link your app. On the other hand, I think I hardcoded compilation of libm anyway so maybe it makes no difference).
make CROSS=rvex-
to build the uclibc library. Then, go back up to the uCLinux directory and do:
make menuconfig
to start the menu to configure uCLinux. You can leave most settings alone, but you need to select TUDelft VEX as the architecture and enable the FLAT binary format. For the library, choose uclibc. Then, you can configure which apps you would like to build. You can add your app in one of these lists and enable it, please check the uCLinux doc on how to do this.
Then:
make
To build linux, the applications and the filesystem. Even after the filesystem has been built, you can add a program into the romfs directory (this is where the filesystem image is being generated from). After that, run
make image
to regenerate the filesystem. I have added an ugly rule in the TUDelft vendor Makefile that tries to run elf2flt on every file in the /bin/ directory (if the file is already in the bflt format, the conversion fails so nothing happens), so you can also copy an ordinary elf file there.

By default, uCLinux generates a romfs filesystem (which is read-only). If you want an ext2 filesystem (read-write), you will need to create it manually (or use Anthony's script) by creating a 4MB file using dd, mount it using a loop device (mount -lo), format it with ext2, copy all the files into the filesystem and make all the device nodes from the templates in the /dev/ directory (using mknod). Anthony's script to do all of this looks like this:
#!/bin/bash
rm -f ext2r0
dd if=/dev/zero of=ext2r0 bs=1M count=4
sudo mkfs.ext2 -r 0 ext2r0
sudo mount ext2r0 ext2fsmount
sudo chown -R aacbrandon ext2fsmount

rm -rf ext2fsmount/*
cp -r romfs/* ext2fsmount/
# fix device nodes
cd ext2fsmount/dev
files=`find . -name '@*'`
for file in $files;
do
    name=`echo $file | cut -d',' -f1 - | tr -d '@'`
    dtype=`echo $file | cut -d',' -f2 - | tr -d '@'`
    major=`echo $file | cut -d',' -f3 - | tr -d '@'`
    minor=`echo $file | cut -d',' -f4 - | tr -d '@'`
    sudo mknod $name $dtype $major $minor
done
rm $files
cd ../..
sudo umount ext2fsmount

Now, you will have a linux-2.0.x/linux.bin (the kernel) and images/romfs.img (the filesystem) file. These must be downloaded to the board at respectively address 0 and 0x800000 (which is where the kernel expects the initial ramdisk initrd). Reset and halt the core before you do so. A GRMON batch upload script looks like this:
load linux-2.0.x/linux.bin
load images/romfs.img 0x8000000

Then, connect to the UART using the rvex-rewrite monitor-nobuf (so it does not use line buffering), and start the core using rvd c.

Alternatively, you can simulate the rVEX running Linux using command
simrvex images/image.elf --ram 0x8000000,images/romfs.img
Additionally, you can add the elf file of your program to the simulator so that you can view the symbols in the trace.
To do this, you must first run simulate once to see where Linux places the program in memory.
Then, add a line to the Makefile of the program with the flag -Wl,--section-start,.init=0x1e80060 (or whatever address).
Add the resulting file to the simulation using the following flag:
simrvex images/image.elf --ram 0x8000000,images/romfs.img --ramelf file_with_start_addr_0x1e80060
A full trace can be performed like this:
simrvex images/image.elf --ram 0x8000000,images/romfs.img -t --ramelf user/pthreadTest/pthreadTest0x1e00060 --ramelf user/sash/sh1e80060 --trace-start-addr 0x1e80060 > simtrace
This command instructs the simulator to trace execution, start the trace the shell starts (so the startup is fast and does not clog the trace file),
, adds 2 the symbols of 2 programs to the trace (the shell and a program for testing pthreads), and redirects stdout to a file. This output will contain the trace, that you can later view with less. The simulation output is written to stderr, so you can view it on you terminal as normal.
stdin will also work as normal, so you can still type commands and start Linux programs while the trace is running. 
Keep in mind that this will rapidly fill up your harddrive.
To speed up simulation if you want to trace a problem that crashes the simulation, you can also enable the circular buffer. It will translate the last x megabytes (you can specify the amount on the cmdline) into human-readable form when the simulation exist.

NOTE ABOUT SIGNALS
When using uCLibc with signals enabled together with pthreads (signals are required for pthreads):
pthreads has a wrapper around sigaction, that will write a user-installed handler into its own handler array in addition to forwarding the sigaction to libc (which will perform the syscall to set the sigaction in the kernel).
This causes a problem with execve (for example, this is what the shell performs when executing a command), because the system call will poperly update the handler of the child process, but the pthreads wrapper will incorrectly also update its own handler array. When the signal is forwarded to the parent, it will find the pthread signal handler wrapper, who will then incorrectly use the updated handler of the child.
A problem quickly arises here as the child will oftenly set the handlers back to default (which is a NULL pointer). For the child, this value is caught in the kernel and dealt with appropriately. For the parent, the kernel still has the original handler installed (which is the pthreads wrapper), but the pthreads wrapper will look up the handler in its array. This entry has been overwritten by the child's call to sigaction and will result in a call to the NULL pointer.
A way to circumvent this is to explicitly use __libc_sigaction() instead of sigaction() (or even signal()) after performing an execve.
