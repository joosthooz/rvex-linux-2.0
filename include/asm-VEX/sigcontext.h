#ifndef _ASM_ST200_SIGCONTEXT_H
#define _ASM_ST200_SIGCONTEXT_H

#include <asm/ptrace.h>

struct sigcontext {
	unsigned long		sc_flags;
	struct pt_regs		sc_regs;	/* registers to restore */
	struct switch_stack sc_ss;	/* registers to restore */
	stack_t			sc_stack;	/* previously active stack */
	unsigned long		sc_reserved[8];
	/*
	 * The mask must come last so we can increase _NSIG_WORDS
	 * without breaking binary compatibility.
	 */
	sigset_t		sc_mask;	/* signal mask to restore after handler returns */
};

//H copied here from linux 2.6 asm-generic
#ifdef __KERNEL__
typedef struct siginfo {
	int si_signo;
	int si_errno;
	int si_code;

//H All of this stuff comes from a newer kernel version so most of it doesn't work
#if 0
	union {
		int _pad[SI_PAD_SIZE];

		/* kill() */
		struct {
			__kernel_pid_t _pid;	/* sender's pid */
			__ARCH_SI_UID_T _uid;	/* sender's uid */
		} _kill;

		/* POSIX.1b timers */
		struct {
			__kernel_timer_t _tid;	/* timer id */
			int _overrun;		/* overrun count */
			char _pad[sizeof( __ARCH_SI_UID_T) - sizeof(int)];
			sigval_t _sigval;	/* same as below */
			int _sys_private;       /* not to be passed to user */
		} _timer;

		/* POSIX.1b signals */
		struct {
			__kernel_pid_t _pid;	/* sender's pid */
			__ARCH_SI_UID_T _uid;	/* sender's uid */
			sigval_t _sigval;
		} _rt;

		/* SIGCHLD */
		struct {
			__kernel_pid_t _pid;	/* which child */
			__ARCH_SI_UID_T _uid;	/* sender's uid */
			int _status;		/* exit code */
			__kernel_clock_t _utime;
			__kernel_clock_t _stime;
		} _sigchld;

		/* SIGILL, SIGFPE, SIGSEGV, SIGBUS */
		struct {
			void __user *_addr; /* faulting insn/memory ref. */
#ifdef __ARCH_SI_TRAPNO
			int _trapno;	/* TRAP # which caused the signal */
#endif
			short _addr_lsb; /* LSB of the reported address */
		} _sigfault;

		/* SIGPOLL */
		struct {
			__ARCH_SI_BAND_T _band;	/* POLL_IN, POLL_OUT, POLL_MSG */
			int _fd;
		} _sigpoll;
	} _sifields;

#endif

} siginfo_t;
#endif

#endif /* _ASM_ST200_SIGCONTEXT_H */



#if 0


#ifndef _ASMsparc_SIGCONTEXT_H
#define _ASMsparc_SIGCONTEXT_H

#include <asm/ptrace.h>

#define SUNOS_MAXWIN   31

#ifndef __ASSEMBLY__

/* SunOS system call sigstack() uses this arg. */
struct sunos_sigstack {
	unsigned long sig_sp;
	int onstack_flag;
};

/* This is what SunOS does, so shall I. */
struct sigcontext_struct {
	int sigc_onstack;      /* state to restore */
	int sigc_mask;         /* sigmask to restore */
	int sigc_sp;           /* stack pointer */
	int sigc_pc;           /* program counter */
	int sigc_npc;          /* next program counter */
	int sigc_psr;          /* for condition codes etc */
	int sigc_g1;           /* User uses these two registers */
	int sigc_o0;           /* within the trampoline code. */

	/* Now comes information regarding the users window set
	 * at the time of the signal.
	 */
	int sigc_oswins;       /* outstanding windows */

	/* stack ptrs for each regwin buf */
	char *sigc_spbuf[SUNOS_MAXWIN];

	/* Windows to restore after signal */
	struct reg_window sigc_wbuf[SUNOS_MAXWIN];
};
#endif /* !(__ASSEMBLY__) */

#endif /* !(_ASMsparc_SIGCONTEXT_H) */

#endif
