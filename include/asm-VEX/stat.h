#ifndef _ASM_ST200_STAT_H
#define _ASM_ST200_STAT_H

//H was stat, but SPARC had an old and new one
struct new_stat {
	unsigned long st_dev;
	unsigned long st_ino;
	unsigned long st_nlink;
	unsigned int st_mode;
	unsigned int st_uid;
	unsigned int st_gid;
	unsigned int __pad0;
	unsigned long st_rdev;
	unsigned long st_size;
	unsigned long st_atime;
	unsigned long st_atime_nsec;
	unsigned long st_mtime;
	unsigned long st_mtime_nsec;
	unsigned long st_ctime;
	unsigned long st_ctime_nsec;
	unsigned long st_blksize;
	long st_blocks;
	unsigned long __unused[3];
};

struct old_stat {
	unsigned short st_dev;
	unsigned short st_ino;
	unsigned short st_mode;
	unsigned short st_nlink;
	unsigned short st_uid;
	unsigned short st_gid;
	unsigned short st_rdev;
	unsigned long  st_size;
	unsigned long  st_atime;
	unsigned long  st_mtime;
	unsigned long  st_ctime;
};
/* SPARC
struct new_stat {
	unsigned short st_dev;
	unsigned short __pad1;
	unsigned long st_ino;
	unsigned short st_mode;
	unsigned short st_nlink;
	unsigned short st_uid;
	unsigned short st_gid;
	unsigned short st_rdev;
	unsigned short __pad2;
	unsigned long  st_size;
	unsigned long  st_blksize;
	unsigned long  st_blocks;
	unsigned long  st_atime;
	unsigned long  __unused1;
	unsigned long  st_mtime;
	unsigned long  __unused2;
	unsigned long  st_ctime;
	unsigned long  __unused3;
	unsigned long  __unused4;
	unsigned long  __unused5;
};
*/

#define STAT_HAVE_NSEC 1

/* This matches struct stat64 in glibc2.1, hence the absolutely
 * insane amounts of padding around dev_t's.
 */
struct stat64 {
	unsigned short st_dev;
	unsigned char __pad0[10];

#define STAT64_HAS_BROKEN_ST_INO	1
	unsigned long __st_ino;

	unsigned int st_mode;
	unsigned int st_nlink;

	unsigned long st_uid;
	unsigned long st_gid;

	unsigned short st_rdev;
	unsigned char __pad3[10];

	long long st_size;
	unsigned long st_blksize;

	long long st_blocks;	/* Number 512-byte blocks allocated. */

	unsigned long st_atime;
	unsigned long st_atime_nsec;

	unsigned long st_mtime;
	unsigned int st_mtime_nsec;

	unsigned long st_ctime;
	unsigned long st_ctime_nsec;

	unsigned long long st_ino;
};

#endif /* _ASM_ST200_STAT_H */
