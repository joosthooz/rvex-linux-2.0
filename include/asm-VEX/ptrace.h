/*
 * Taken from the m68k port.
 *
 * Copyright (C) 2004, Microtronix Datacom Ltd.
 *
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 * NON INFRINGEMENT.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifndef _VEX_PTRACE_H
#define _VEX_PTRACE_H


/*
 * Register numbers used by 'ptrace' system call interface.
 */

/* copied from sparc, I did not check all offsets -- piet */

/* First generic task_struct offsets. sizeof(task_struct)=1576 */
#define TASK_STATE        0x000
//counter
#define TASK_PRIORITY     0x008
#define TASK_SIGNAL       0x00c
#define TASK_BLOCKED      0x010
#define TASK_FLAGS        0x014
#define TASK_SAVED_KSTACK 0x054
#define TASK_KSTACK_PG    0x058

/* Thread stuff. */
#define THREAD_UMASK      0x210
#define THREAD_SADDR      0x218
#define THREAD_SDESC      0x21c
#define THREAD_KSP        0x220
#define THREAD_KPC        0x224
#define THREAD_KPSR       0x228
#define THREAD_KWIM       0x22c
#define THREAD_FORK_KPSR  0x230
#define THREAD_FORK_KWIM  0x234
#define THREAD_REG_WINDOW 0x238
#define THREAD_STACK_PTRS 0x438
#define THREAD_W_SAVED    0x458
#define THREAD_FLOAT_REGS 0x460
#define THREAD_FSR        0x560
#define THREAD_SIGSTK     0x5e8
#define THREAD_MM     0x620
#define THREAD_MM_CTX     0x008

/* Text/data offsets, needed by gdbserver */
#define PT_TEXT_ADDR    44*4
#define PT_TEXT_END_ADDR 45*4
#define PT_DATA_ADDR    46*4

#ifndef __ASSEMBLY__

/* this struct defines the way the registers are stored on the
   stack during a system call.

   There is a fake_regs in setup.c that has to match pt_regs.*/

struct pt_regs {

    unsigned long  sccr;    /* rVEX Context Control Register */
    unsigned long  pc;        /* Program counter */

    unsigned long  r1;        /* Stack pointer */
    unsigned long  r2;        /* Retval LS 32bits */
    unsigned long  r3;        /* r3-r10 Register arguments */
    unsigned long  r4;
    unsigned long  r5;
    unsigned long  r6;
    unsigned long  r7;
    unsigned long  r8;
    unsigned long  r9;
    unsigned long  r10;

    unsigned long  r11;        /* r11-r15 Caller-saved GP registers */
    unsigned long  r12;
    unsigned long  r13;
    unsigned long  r14;
    unsigned long  r15;

    unsigned long  r16;        /* r16-56 Scratch temporaries (user mode only) */
    unsigned long  r17;
    unsigned long  r18;
    unsigned long  r19;
    unsigned long  r20;
    unsigned long  r21;
    unsigned long  r22;
    unsigned long  r23;
    unsigned long  r24;
    unsigned long  r25;
    unsigned long  r26;
    unsigned long  r27;
    unsigned long  r28;
    unsigned long  r29;
    unsigned long  r30;
    unsigned long  r31;
    unsigned long  r32;
    unsigned long  r33;
    unsigned long  r34;
    unsigned long  r35;
    unsigned long  r36;
    unsigned long  r37;
    unsigned long  r38;
    unsigned long  r39;
    unsigned long  r40;
    unsigned long  r41;
    unsigned long  r42;
    unsigned long  r43;
    unsigned long  r44;
    unsigned long  r45;
    unsigned long  r46;
    unsigned long  r47;
    unsigned long  r48;
    unsigned long  r49;
    unsigned long  r50;
    unsigned long  r51;
    unsigned long  r52;
    unsigned long  r53;
    unsigned long  r54;
    unsigned long  r55;
    unsigned long  r56;

//    unsigned long  r57;  /* 57 - 63 callee-saved registers (63 not used, 62 Frame Pointer)*/
//    unsigned long  r58;
//    unsigned long  r59;
//    unsigned long  r60;
//    unsigned long  r61;
//    unsigned long  r62;    /* Frame pointer */
//    unsigned long  r63;

    unsigned char  br; /* Branch registers stored as single byte */

    unsigned long  lr;        /* Linker register */
    unsigned long padding[4]; /* Padd structure to 0x100 bytes. */
};

/*
 * Like the ST200 port, we're saving the callee saved regs in a struct that is stored
 * below the pt_regs. __switch_to uses it.
 */
struct switch_stack
{
    unsigned long  r57;  /* 57 - 63 callee-saved registers (63 not used, 62 Frame Pointer)*/
    unsigned long  r58;
    unsigned long  r59;
    unsigned long  r60;
    unsigned long  r61;
    unsigned long  r62;
//    unsigned long padding[2]; /* pad to 0x20 bytes */
};


#ifdef __KERNEL__
/* Arbitrarily choose the same ptrace numbers as used by the Sparc code. */
#define PTRACE_GETREGS            12
#define PTRACE_SETREGS            13

#ifndef PS_S
#define PS_S  (0x00000001)
#endif
#ifndef PS_T
#define PS_T  (0x00000002)
#endif

/* #define user_mode(regs) (!((regs)->status_extension & PS_S)) */
#define user_mode(regs) 0 //H todo check if necessary, we dont have user/system modes so we alway have to save everything. But how about different (user/kernel) stacks? need to test.
#define instruction_pointer(regs) ((regs)->pc)
#define profile_pc(regs) instruction_pointer(regs)
extern void show_regs(struct pt_regs *);

#endif /* __KERNEL__ */
#endif /*__ASSEMBLY__*/
#endif /* _VEX_PTRACE_H */
