/*
 *  include/asm-st200/syscallparams.h
 *
 *  Copyright (C) 2003 STMicroelectronics Limited
 *      Author: Stuart Menefy <stuart.menefy@st.com>
 *
 */

#ifndef _ASM_ST200_SYSCALLPARAM_H
#define _ASM_ST200_SYSCALLPARAM_H

#include "asm/ptrace.h"
/*
 * The LX ABI specifies that the first eight parameters to a function are
 * passed in registers, the remaining parameters are passed on the stack.
 * This allows us to 'pass' the registers pushed onto the stack by the
 * system call entry code to the C code. However there are some restrictions
 * on the alignment of these parameters, there is the 16 byte scratch area
 * which every function has, and some additional padding needed to align
 * the stack on a 32 byte boundary. So by defining these structures,
 * and passing them as the 9th parameter, we can make use of this facility,
 * and avoid the above problems.
 */

struct syscallparams {
	unsigned int pad[4];
	struct pt_regs regs;
};
struct syscallparams_ss {
	unsigned int pad[4];
	struct switch_stack ss;
	struct pt_regs regs;
};

/*
 * This demonstrates how it is possible to return two results from a
 * system call.
 */
union syscallresult {
	unsigned long result1;
	struct {
		unsigned long r16;
		unsigned long r17;
	} result2;
};

#endif /* _ASM_ST200_SYSCALLPARAM_H */
