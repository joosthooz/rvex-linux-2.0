/*
 * include/asm-st200/pgtable-nommu.h
 *
 * Copyright (C) 2002 STMicroelectronics Limited
 *	Author: Stuart Menefy <stuart.menefy@st.com>
 *
 */

#ifndef _ASM_ST200_PGTABLE_NOMMU_H
#define _ASM_ST200_PGTABLE_NOMMU_H

#include <linux/config.h>

#ifndef __ASSEMBLY__

//#include <linux/slab.h>
#include <asm/processor.h>
#include <asm/page.h>
#include <asm/io.h>


typedef pte_t *pte_addr_t;


/*
 * procfs/kcore needs it
 */
#define VMALLOC_START	0
#define VMALLOC_END	(~0)


/*
 * Trivial page table functions.
 */
#define pgd_present(pgd)	(1)
#define pgd_none(pgd)		(0)
#define pgd_bad(pgd)		(0)
#define pgd_clear(pgdp)
#define kern_addr_valid(addr)	(1)
#define	 pmd_offset(a, b)	((void *)0)

#define PAGE_NONE	__pgprot(0)
#define PAGE_SHARED	__pgprot(0)
#define PAGE_COPY	__pgprot(0)
#define PAGE_READONLY	__pgprot(0)
#define PAGE_KERNEL	__pgprot(0)

//extern void paging_init(void);	//H already defined
#define swapper_pg_dir ((pgd_t *) 0)

#define __swp_type(x)		(0)
#define __swp_offset(x)		(0)
#define __swp_entry(typ,off)	((swp_entry_t) { ((typ) | ((off) << 7)) })
#define __pte_to_swp_entry(pte)	((swp_entry_t) { pte_val(pte) })
#define __swp_entry_to_pte(x)	((pte_t) { (x).val })

//H from st200 linux 2.6 kernel (cacheflush.h)
#define flush_cache_mm(mm)			do { } while (0)

/*
 * ZERO_PAGE is a global shared page that is always zero: used
 * for zero-mapped memory areas etc..
 * Should never be used, so try and cause a bus error if it is.
 */
#define ZERO_PAGE(vaddr)	((void *)0x87654321)

extern unsigned int kobjsize(const void *objp);

/*
 * No page table caches to initialise.
 */
#define pgtable_cache_init()	do { } while (0)
#define io_remap_page_range	remap_page_range
  
//   extern unsigned long empty_zero_page[PAGE_SIZE/sizeof(unsigned long)];		//H already defined
/* Which definition do we keep ? 
   #define ZERO_PAGE(vaddr) (virt_to_page(empty_zero_page))
*/

/* Push the page at kernel virtual address and clear the icache */
extern inline void flush_page_to_ram (unsigned long address)
{
}

/* Push n pages at kernel virtual address and clear the icache */
extern inline void flush_pages_to_ram (unsigned long address, int n)
{
}

extern int sigrestorer_start(void);
#endif /* !__ASSEMBLY__ */
#endif /* _ASM_ST200_PGTABLE_NOMMU_H */
