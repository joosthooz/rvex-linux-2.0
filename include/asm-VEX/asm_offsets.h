/* Copyright 2013 Joost Hoozemans, TU Delft
 *
 *
 *
 */

#ifndef ASM_OFFSETS_H_
#define ASM_OFFSETS_H_

/* Offsets for pt_regs struct. */
#define TRACEREG_SZ   0x100

#define PT_SCCR (0)
#define PT_PC (4)
#define PT_R(x) ((x-1)*4 + 2*4)
#define PT_BR ((56+2)*4)
#define PT_LR (PT_BR + 4)

/* offsets into thread_struct */

#define THREAD_STRUCT_SP 0x0
#define THREAD_STRUCT_PC 0x4
#define THREAD_STRUCT_FLAGS 0x8

/* These do not belong here but I have nowhere else to put them */

/* thread information allocation */
#define THREAD_SIZE_ORDER	1
#define THREAD_SIZE	(PAGE_SIZE << THREAD_SIZE_ORDER)

/*
 * thread information flags
 * - these are process state flags that various assembly files may need to access
 * - pending work-to-be-done flags are in lower bits (faster to access)
 * - other flags in higher bits
 */
#define TIF_SYSCALL_TRACE	0	/* syscall trace active */
#define TIF_NOTIFY_RESUME	1	/* resumption notification requested */
#define TIF_SIGPENDING		2	/* signal pending */
#define TIF_NEED_RESCHED	3	/* rescheduling necessary */
#define TIF_POLLING_NRFLAG	16	/* true if poll_idle() is polling TIF_NEED_RESCHED */
#define TIF_MEMDIE              17

#define _TIF_SYSCALL_TRACE	(1<<TIF_SYSCALL_TRACE)
#define _TIF_NOTIFY_RESUME	(1<<TIF_NOTIFY_RESUME)
#define _TIF_SIGPENDING		(1<<TIF_SIGPENDING)
#define _TIF_NEED_RESCHED	(1<<TIF_NEED_RESCHED)
#define _TIF_POLLING_NRFLAG	(1<<TIF_POLLING_NRFLAG)

#define _TIF_WORK_MASK		0x0000FFFE	/* work to do on interrupt/exception return */
#define _TIF_ALLWORK_MASK	0x0000FFFF	/* work to do on any return to u-space */

#define STACK_SCRATCH_AREA (8*4)

#define SIZEOF_SWITCH_STACK (6*4) /* sizeof(struct switch_stack) */

#endif /* ASM_OFFSETS_H_ */
