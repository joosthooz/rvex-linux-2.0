/*
 *  include/asm-st200/atomic.h
 *
 *  Copyright (C) 2002 STMicroelectronics Limited
 *	Author: Stuart Menefy <stuart.menefy@st.com>
 *
 */

#ifndef _ASM_ST200_ATOMIC_H
#define _ASM_ST200_ATOMIC_H

#include <asm/system.h>
#ifndef typecheck

/* Gross Hack time. linux/kernel.h now includes bitops.h. 
 * which has the definition of typecheck in it. Unfortunately 
 * we need it for the local_save_flags etc below. Rather than 
 * get rid of the typecheck macro in local_save_flags, I've stuck
 * it in here, because when I figure out why using the atomic rollback
 * technique causes failure
 */

#define typecheck(type,x) \
({	type __dummy; \
	typeof(x) __dummy2; \
	(void)(&__dummy == &__dummy2); \
	1; \
})
#endif

#define save_and_cli(x)  do { local_save_flags(x); local_irq_disable(); } while (0)
//#define restore_flags(x) local_irq_restore(x)

/*
 * Atomic operations that C can't guarantee us.  Useful for
 * resource counting etc..
 *
 */

/*
 * Make sure gcc doesn't try to be clever and move things around
 * on us. We need to use _exactly_ the address the user gave us,
 * not some alias that contains the same information.
 */
typedef struct { volatile int counter; } atomic_t;

#define ATOMIC_INIT(i)	{ (i) }

/**
 * atomic_read - read atomic variable
 * @v: pointer of type atomic_t
 * 
 * Atomically reads the value of @v.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_read(v)		((v)->counter)

/**
 * atomic_set - set atomic variable
 * @v: pointer of type atomic_t
 * @i: required value
 *
 * Atomically sets the value of @v to @i.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_set(v,i)		((v)->counter = (i))

/**
 * atomic_add - add integer to atomic variable
 * @i: integer value to add
 * @v: pointer of type atomic_t
 *
 * Atomically adds @i to @v.  Note that the guaranteed useful range
 * of an atomic_t is only 24 bits.
 */
 extern __inline__ void atomic_add(int i, atomic_t * v)
{
	unsigned long flags;

	save_and_cli(flags);
	*(long *)v += i;
	restore_flags(flags);
}

/**
 * atomic_sub - subtract the atomic variable
 * @i: integer value to subtract
 * @v: pointer of type atomic_t
 *
 * Atomically subtracts @i from @v.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
extern __inline__ void atomic_sub(int i, atomic_t *v)
{
	unsigned long flags;

	save_and_cli(flags);
	*(long *)v -= i;
	restore_flags(flags);
}

/* Helper function */
extern __inline__ int __atomic_add_return(int i, atomic_t * v)
{
	unsigned long temp, flags;

	save_and_cli(flags);
	temp = *(long *)v;
	temp += i;
	*(long *)v = temp;
	restore_flags(flags);

	return temp;
}

/* Helper function */
extern __inline__ int __atomic_sub_return(int i, atomic_t * v)
{
	unsigned long temp, flags;

	save_and_cli(flags);
	temp = *(long *)v;
	temp -= i;
	*(long *)v = temp;
	restore_flags(flags);

	return temp;
}

/**
 * atomic_sub_and_test - subtract value from variable and test result
 * @i: integer value to subtract
 * @v: pointer of type atomic_t
 *
 * Atomically subtracts @i from @v and returns
 * true if the result is zero, or false for all
 * other cases.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_sub_and_test(i,v) (__atomic_sub_return((i), (v)) == 0)

/**
 * atomic_inc - increment atomic variable
 * @v: pointer of type atomic_t
 *
 * Atomically increments @v by 1.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_inc(v) atomic_add(1,(v))

/**
 * atomic_dec - decrement atomic variable
 * @v: pointer of type atomic_t
 *
 * Atomically decrements @v by 1.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_dec(v) atomic_sub(1,(v))

/**
 * atomic_dec_and_test - decrement and test
 * @v: pointer of type atomic_t
 *
 * Atomically decrements @v by 1 and
 * returns true if the result is 0, or false for all other
 * cases.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_dec_and_test(v) (__atomic_sub_return(1, (v)) == 0)

/**
 * atomic_inc_and_test - increment and test
 * @v: pointer of type atomic_t
 *
 * Atomically increments @v by 1
 * and returns true if the result is zero, or false for all
 * other cases.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_inc_and_test(v) (__atomic_add_return(1, (v)) == 0)

/**
 * atomic_add_negative - add and test if negative
 * @v: pointer of type atomic_t
 * @i: integer value to add
 *
 * Atomically adds @i to @v and returns true
 * if the result is negative, or false when
 * result is greater than or equal to zero.  Note that the guaranteed
 * useful range of an atomic_t is only 24 bits.
 */
#define atomic_add_negative(i, v) (__atomic_add_return(i, (v)) < 0)

/* Helper functions for semaphores */
#define atomic_dec_return(v)	__atomic_sub_return (1, (v))
#define atomic_inc_return(v)	__atomic_add_return (1, (v))

/* Atomic operations are already serializing */
#define smp_mb__before_atomic_dec()	barrier()
#define smp_mb__after_atomic_dec()	barrier()
#define smp_mb__before_atomic_inc()	barrier()
#define smp_mb__after_atomic_inc()	barrier()

#undef save_and_cli
//#undef restore_flags

#endif /* _ASM_ST200_ATOMIC_H */
