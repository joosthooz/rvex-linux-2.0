#ifndef __ASM_ST200_SYSTEM_H
#define __ASM_ST200_SYSTEM_H

#include <linux/config.h>

#include <asm/segment.h>
#include <asm/ptrace.h>
//#include <asm/ctrlregdef.h>
#include <asm/rvex.h>

#define switch_to(prev, next) do { \
                          __switch_to(prev, next); \
                     } while(0)



/* FIXME : We need to issue a sync() call somehow. There is an intrinsic to
 * do this, alternatively we call to assembler. I've stuck in a function
 * call for now.
 */
extern void __sync(void);

#define mb()		__sync()
#define rmb()		mb()
#define wmb()		mb()
#define smp_mb()	mb()
#define smp_rmb()	mb()
#define smp_wmb()	mb()
#define smp_read_barrier_depends()	do { } while(0)
#define read_barrier_depends()		do { } while(0)

#define set_mb(var, value)		do { var = value; mb(); } while (0)
#define set_wmb(var, value)		do { var = value; wmb(); } while (0)

/* interrupt control.. */
#define cli()		DISABLE_IRQ
#define sti()		ENABLE_IRQ

#define local_irq_disable() DISABLE_IRQ
#define local_irq_enable() ENABLE_IRQ

#define local_save_flags(x) do { x = CR_CCR; } while(0)
#define local_irq_restore(x) setipl(x)



/* For spinlocks etc */
#define local_irq_save(x)  do { local_save_flags(x); local_irq_disable(); } while (0)


extern inline void setipl(int newipl)
{
	int tmp = CR_CCR;
	// Interupt bits are lower four bits
	tmp |= (newipl);
	CR_CCR = tmp;
}

#define save_flags(flags)	local_save_flags(flags)
#define restore_flags(x) local_irq_restore(x)

/* For spinlocks etc */
#define local_irq_save(x)  do { local_save_flags(x); local_irq_disable(); } while (0)



/* Trap flag enable/disable */

extern inline void traps_enable()
{
	ENABLE_TRAPS;
}

extern inline void traps_disable()
{
	DISABLE_TRAPS;
}


#define xchg(ptr,v) ((__typeof__(*(ptr)))__xchg((unsigned long)(v),(ptr),sizeof(*(ptr))))

/* This function doesn't exist, so you'll get a linker error
   if something tries to do an invalid cmpxchg().  */
extern void __xchg_called_with_bad_pointer(void);


#define nop() __asm__ __volatile__ ("c0 nop\n;;\n\t");

extern inline unsigned long xchg_u32(volatile unsigned long *m, unsigned long val)
{
	unsigned long flags, retval;

	save_flags(flags); cli();
	retval = *m;
	*m = val;
	restore_flags(flags);
	return retval;
}

#define tas(ptr) (xchg((ptr),1))

extern inline unsigned long __xchg(unsigned long x, volatile void * ptr, int size)
{
	switch (size) {
	case 4:
		return xchg_u32(ptr, x);
	};
	__xchg_called_with_bad_pointer();
	return x;
}

extern void die_if_kernel(const char *str, struct pt_regs *regs) __attribute__ ((noreturn));

#endif /* __ASM_ST200_SYSTEM_H */
